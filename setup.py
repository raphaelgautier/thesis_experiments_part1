from setuptools import setup

setup(
    name="ThesisExperimentsPart1",
    version="0.0.1",
    url="https://gitlab.com/raphaelgautier/thesis_experiments_part1",
    author="Raphael Gautier",
    author_email="raphael.gautier@gatech.edu",
    description="First part of the code implementing thesis experiments.",
    packages=["bsdr"],
    install_requires=[],
)
