from doe_scheduler.server import DoESchedulerServer

from bsdr import doe_name, cases_list

server = DoESchedulerServer(doe_name, cases_list)
server.start()
