from itertools import product

from matplotlib import pyplot as plt
import seaborn as sns
from tqdm import tqdm
from pandas import read_csv

from bsdr import data_dir

# Load results #########################################################################
results = read_csv(data_dir / "results_final" / "compiled_results.csv")

metrics = ["r_squared", "mlppd"]
dataset_outputs = (
    results[["dataset_name", "output_name", "dim_feature_space"]]
    .drop_duplicates()
    .values.tolist()
)
for (dataset_name, output_name, dim_feature_space), metric in tqdm(
    list(product(dataset_outputs, metrics))
):
    data = results[
        (results["dataset_name"] == dataset_name)
        & (results["output_name"] == output_name)
        & (results["dim_feature_space"] == dim_feature_space)
    ]

    sns.boxplot(
        x="num_training_samples",
        y=metric,
        hue="model_name",
        data=data,
        # ax=ax,
        linewidth=0.5,
        width=0.7,
        flierprops=dict(markerfacecolor="0.50", markersize=2),
    )

    if metric == "r_squared":
        plt.ylim([-0.1, 1.1])

    if metric == "mlppd":
        bfs_bgp_data = data[
            (data["model_name"] == "bfs") | (data["model_name"] == "bgp")
        ]
        mmin = bfs_bgp_data["mlppd"].min()
        mmax = bfs_bgp_data["mlppd"].max()
        plt.ylim([mmin, mmax])

    plt.grid()

    folder = data_dir / "figures" / f"{dataset_name}"
    folder.mkdir(parents=True, exist_ok=True)

    plt.savefig(folder / f"{output_name}_{dim_feature_space}dFS_{metric}.png")
    plt.close()
