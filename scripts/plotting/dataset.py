from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.linalg import svd

from bsdr.utils import load_dataset

dataset_name = "RAE2822_baseline_51DV_M0.725"
# dataset_name = "CRM_450kgrid_twist_50DV_subsonic"
output_name = "lift"

x, y, dy_dx = load_dataset(dataset_name, output_name)
# sns.pairplot(pd.DataFrame(x[:, 20:30]), corner=True)

U, s, Vh = svd((x - np.mean(x, axis=0)) / np.std(x, axis=0), full_matrices=False)
print(s)

print(U[0, :])

# plt.hist(y)

# plt.show()


from pyDOE2 import lhs

x = lhs(51, 2000)
print(x.shape)
U, s, Vh = svd(x - np.mean(x, axis=0), full_matrices=False)
print(s)
# sns.pairplot(pd.DataFrame(x[:, 40:51]), corner=True)
# plt.show()
