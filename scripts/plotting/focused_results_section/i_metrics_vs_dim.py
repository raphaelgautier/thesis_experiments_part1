from matplotlib import pyplot as plt
import pandas as pd
import seaborn as sns

from bsdr import data_dir

dataset_name = "onera_m6"
output_name = "lift"
file_path = data_dir / "results" / "final" / "compiled_results.csv"
list_num_training_samples = [100, 250]

# Read file
df = pd.read_csv(file_path, index_col=0)

# Display
metric_labels = {
    "r_squared": "$R^2$",
    "mlppd": "Mean Log Pointwise\nPredictive Density",
}

# Matplotlib
plt.rcParams.update({"font.size": 8, "text.usetex": False, "font.serif": "Arial"})
FONT_SIZE = 8
plt.rc("font", size=FONT_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=6)  # fontsize of the tick labels
plt.rc("legend", fontsize=FONT_SIZE)  # legend fontsize
plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title

# Plot
for num_training_samples in list_num_training_samples:
    for metric in ["r_squared", "mlppd"]:
        plt.figure(figsize=(3, 1.5))
        sns.boxplot(
            data=df[
                (df["num_training_samples"] == num_training_samples)
                & (df["dataset_name"] == dataset_name)
                & (df["output_name"] == output_name)
                & (df["model_name"] == "bfs")
            ],
            x="dim_feature_space",
            y=metric,
        )
        plt.xlabel("$m$")
        plt.ylabel(metric_labels[metric])
        plt.grid()
        plt.tight_layout()
        # plt.show()

        plt.savefig(
            data_dir
            / "figures_eps"
            / "metrics"
            / f"{dataset_name}_{output_name}_{metric}_vsD_{num_training_samples}TS.eps"
        )
