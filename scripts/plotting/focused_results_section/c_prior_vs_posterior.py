import h5py
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from bsdr import data_dir

# Script Parameters
subfolder = "oneram6_longer_chains"
dataset_name = "onera_m6"
cases = [
    "bfs_onera_m6_lift_1dFS_100TS_RS867",
    "bfs_onera_m6_lift_1dFS_250TS_RS867",
    "bfs_onera_m6_lift_5dFS_100TS_RS867",
    "bfs_onera_m6_lift_5dFS_250TS_RS867",
]
num_max_disp_proj_params = 5
# num_prior_samples = 1000

param_labels = {
    "noise_variance": "\\sigma_n",
    "signal_variance": "\\sigma_f",
    "length_scale_0": "\\ell_0",
    "length_scale_1": "\\ell_1",
    "length_scale_2": "\\ell_2",
    "length_scale_3": "\\ell_3",
    "length_scale_4": "\\ell_4",
    "projection_parameter_0": "\\theta_{{p,0}}",
    "projection_parameter_1": "\\theta_{{p,1}}",
    "projection_parameter_2": "\\theta_{{p,2}}",
    "projection_parameter_3": "\\theta_{{p,3}}",
    "projection_parameter_4": "\\theta_{{p,4}}",
}

# Create one plot for each case
for case in cases:

    # Asemble file path
    file_path = data_dir / "results" / subfolder / dataset_name / f"{case}.h5"

    # Matplotlib settings
    plt.rcParams.update({"font.size": 8, "text.usetex": False, "font.serif": "Arial"})
    sns.set(style="ticks")
    FONT_SIZE = 8
    plt.rc("font", size=FONT_SIZE)  # controls default text sizes
    plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
    plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
    plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
    plt.rc("ytick", labelsize=6)  # fontsize of the tick labels
    plt.rc("legend", fontsize=FONT_SIZE)  # legend fontsize
    plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title

    # Prepare plot
    with h5py.File(file_path, "r") as h5_file:

        # Retrieve chains
        posterior_draws = h5_file["training_artifacts"]["posterior_draws"]
        num_length_scales = posterior_draws["length_scales"].shape[1]
        num_projection_parameters = min(
            num_max_disp_proj_params, posterior_draws["projection_parameters"].shape[1]
        )

        # Parameter names
        param_names = (
            ["signal_variance", "noise_variance"]
            + [f"length_scale_{i}" for i in range(num_length_scales)]
            + [f"projection_parameter_{i}" for i in range(num_projection_parameters)]
        )

        # Total number of parameters/individual plots
        num_params = 2 + num_length_scales + num_projection_parameters

        # Assemble posteriors into a single pandas dataframe (easier for plotting)
        posteriors = pd.DataFrame(
            data=np.concatenate(
                (
                    np.array(posterior_draws["signal_variance"])[:, None],
                    np.array(posterior_draws["noise_variance"])[:, None],
                    posterior_draws["length_scales"],
                    posterior_draws["projection_parameters"][
                        :, :num_projection_parameters
                    ],
                ),
                axis=1,
            ),
            columns=param_names,
        )

        # Corresponding priors
        num_prior_samples = posterior_draws["signal_variance"].shape[0]
        priors = pd.DataFrame(
            data=np.concatenate(
                (
                    np.random.lognormal(
                        size=(num_prior_samples, 2 + num_length_scales)
                    ),
                    np.random.normal(
                        size=(num_prior_samples, num_projection_parameters)
                    ),
                ),
                axis=1,
            ),
            columns=param_names,
        )

        # Actually plot
        if num_params <= 8:
            plt.figure(figsize=(3, 3))
        else:
            plt.figure(figsize=(3, 4))
        for i, param in enumerate(param_names):
            ax = plt.subplot(int(np.ceil(num_params / 2)), 2, i + 1)
            mini = min(
                np.quantile(priors[param], 0.01), np.quantile(posteriors[param], 0.01),
            )
            maxi = max(
                np.quantile(priors[param], 0.99), np.quantile(priors[param], 0.99)
            )

            sns.histplot(
                np.stack((posteriors[param], priors[param]), axis=1),
                ax=ax,
                stat="density",
                binwidth=(maxi - mini) / 30,
                legend=False,
            )
            plt.ylabel(f"${param_labels[param]}$")
            plt.xlabel("")
            ax.set_xlim([mini, maxi])
            ax.tick_params(length=2, pad=3)

        plt.tight_layout()
        plt.subplots_adjust(hspace=0.5)

    plt.savefig(
        data_dir / "figures_eps" / subfolder / "prior_vs_posterior" / f"{case}.eps",
        format="eps",
    )
# plt.show()
