from matplotlib import pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import pandas as pd
import seaborn as sns

from bsdr import data_dir

dataset_name = "onera_m6"
output_name = "lift"
file_path = (
    data_dir / "figures" / "split_gelman_rubin" / f"{dataset_name}_{output_name}.csv"
)
list_num_training_samples = [100, 250]

# Read file
df = pd.read_csv(file_path, index_col=0)

# Order and label for subplots
param_names = [
    {"column": "noise_variance", "symbol": "\\sigma_n"},
    {"column": "signal_variance", "symbol": "\\sigma_f"},
    {"column": "length_scales_0", "symbol": "\\ell_0"},
    {"column": "length_scales_1", "symbol": "\\ell_1"},
    {"column": "length_scales_2", "symbol": "\\ell_2"},
    {"column": "length_scales_3", "symbol": "\\ell_3"},
    {"column": "length_scales_4", "symbol": "\\ell_4"},
    {"column": "projection_parameters_0", "symbol": "\\theta_{{p,0}}"},
    {"column": "projection_parameters_1", "symbol": "\\theta_{{p,1}}"},
    {"column": "projection_parameters_2", "symbol": "\\theta_{{p,2}}"},
    {"column": "projection_parameters_3", "symbol": "\\theta_{{p,3}}"},
    {"column": "projection_parameters_4", "symbol": "\\theta_{{p,4}}"},
]

# Matplotlib
plt.rcParams.update({"font.size": 8, "text.usetex": False, "font.serif": "Arial"})
FONT_SIZE = 8
plt.rc("font", size=FONT_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=6)  # fontsize of the tick labels
plt.rc("legend", fontsize=FONT_SIZE)  # legend fontsize
plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title

for num_training_samples in list_num_training_samples:

    fig, axes = plt.subplots(len(param_names), 1, sharex=True, figsize=(3, 3.5))
    for i, param_info in enumerate(param_names):
        ax = axes[i]

        data = df[df["num_training_samples"] == num_training_samples]
        # sns.scatterplot(
        #     data=data, x="dim_feature_space", y=param_info["column"], ax=ax, s=6, alpha=0.5
        # )
        sns.boxplot(
            data=data,
            x="dim_feature_space",
            y=param_info["column"],
            ax=ax,
            linewidth=0.5,
            width=0.7,
            flierprops=dict(markerfacecolor="0.50", markersize=2),
        )

        print(i, len(param_names) - 1)
        if i == len(param_names) - 1:
            # ax.set_xticks(ticks=[1, 2, 3, 4, 5])
            ax.set_xlabel("$m$")
        else:
            pass
            # ax.set_xticks(ticks=[])

        mini = data[param_info["column"]].min()
        maxi = data[param_info["column"]].max()

        ax.set_yticks([mini, maxi])

        range = maxi - mini
        mini -= 0.15 * range
        maxi += 0.15 * range

        ax.set_ylim([mini, maxi])
        ax.yaxis.set_major_formatter(FormatStrFormatter("%.2f"))
        ax.set_ylabel(f"${param_info['symbol']}$")

    plt.tight_layout()
    plt.subplots_adjust(hspace=0.3, left=0.16, top=0.99, right=0.99)
    # plt.show()
    plt.savefig(
        data_dir
        / "figures_eps"
        / "split_gelman_rubin"
        / f"{dataset_name}_{output_name}_vsdim_{num_training_samples}TS.eps",
        format="eps",
    )
