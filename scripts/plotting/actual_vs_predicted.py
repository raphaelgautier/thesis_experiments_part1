import pathlib

import h5py
from matplotlib import pyplot as plt
import numpy as np

from bsdr.utils import load_dataset


def plot_actual_vs_predicted_asymmetric(
    actual_y,
    predicted_y,
    training_confidence_interval_bounds,
    title=None,
    xlabel="Actual",
    ylabel="Predicted",
    ax=None,
):
    errors = np.concatenate(
        (
            predicted_y - training_confidence_interval_bounds[:, 0:1],
            training_confidence_interval_bounds[:, 1:2] - predicted_y,
        ),
        axis=1,
    ).T

    mmin = min(np.min(actual_y), np.min(predicted_y))
    mmax = max(np.max(actual_y), np.max(predicted_y))
    padding = (mmax - mmin) * 0.1
    bounds = [mmin - padding, mmax + padding]

    if ax is None:
        _, ax = plt.subplots(1, 1)

    ax.errorbar(actual_y, predicted_y, yerr=errors, fmt="o", alpha=0.2)

    ax.set_title("Predicted vs. Actual (Training)")
    ax.plot(bounds, bounds, "--")
    ax.set_aspect(1.0)
    ax.set_xlim(bounds)
    ax.set_ylim(bounds)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    plt.grid()
    if title is not None:
        plt.title(title)
    plt.tight_layout()
    # plt.show()
    # plt.savefig('figures/training_actual_vs_predicted')


def plot_actual_vs_predicted(results_filename):
    # Project's root folder
    root_folder_path = pathlib.Path(__file__).parents[1]

    # Load relevant info from the results file
    with h5py.File(root_folder_path / "results" / results_filename, "r") as h5_file:
        dataset_name = h5_file["case_inputs"]["dataset"].attrs["name"]
        num_inputs = h5_file["case_inputs"]["dataset"].attrs["num_inputs"]
        output_name = h5_file["case_inputs"]["dataset"].attrs["output_name"]

        training_indices = h5_file["case_inputs"]["training_indices"]
        validation_indices = h5_file["case_inputs"]["validation_indices"]

        training_point_based_predictions = h5_file["validation_artifacts"][
            "training_set"
        ]["point_based_predictions"]
        training_confidence_interval_bounds = h5_file["validation_artifacts"][
            "training_set"
        ]["confidence_interval_bounds"]

        validation_point_based_predictions = h5_file["validation_artifacts"][
            "validation_set"
        ]["point_based_predictions"]
        validation_confidence_interval_bounds = h5_file["validation_artifacts"][
            "validation_set"
        ]["confidence_interval_bounds"]

        # Load relevant info from the dataset file
        dataset_path = root_folder_path / "data" / f"{dataset_name}.csv"
        _, y_actual, _ = load_dataset(dataset_path, num_inputs, output_name)

        # Training and validation split
        # training_x = x[training_indices]
        # validation_x = x[validation_indices]
        training_y_actual = y_actual[training_indices]
        validation_y_actual = y_actual[validation_indices]

        print(
            training_y_actual.shape,
            training_point_based_predictions.shape,
            training_confidence_interval_bounds.shape,
        )

        # Plots
        plot_actual_vs_predicted_asymmetric(
            training_y_actual,
            training_point_based_predictions,
            training_confidence_interval_bounds,
            title=f"{h5_file['case_inputs'].attrs['model_name']} - Training",
        )

        plot_actual_vs_predicted_asymmetric(
            validation_y_actual,
            validation_point_based_predictions,
            validation_confidence_interval_bounds,
            title=f"{h5_file['case_inputs'].attrs['model_name']} - Validation",
        )


if __name__ == "__main__":
    filenames = [
        "naca0012/bfs_naca0012_lift_40_training_samples.h5",
        "naca0012/moas_naca0012_lift_40_training_samples.h5",
        "naca0012/bgp_naca0012_lift_40_training_samples.h5",
    ]
    for filename in filenames:
        plot_actual_vs_predicted(filename)

    plt.show()
