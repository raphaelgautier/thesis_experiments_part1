from itertools import product

from matplotlib import pyplot as plt, ticker
from matplotlib.gridspec import GridSpec
import seaborn as sns

from bsdr import data_dir

figures_path = data_dir / "figures_eps" / "parametric_study"

dataset_groups = {
    "quadratic_functions": [
        ("quadratic_function_10_inputs_2_active_dims", "y", 10, 2),
        ("quadratic_function_10_inputs_5_active_dims", "y", 10, 5),
        ("quadratic_function_25_inputs_2_active_dims", "y", 25, 2),
        ("quadratic_function_25_inputs_5_active_dims", "y", 25, 5),
        ("quadratic_function_50_inputs_2_active_dims", "y", 50, 2),
        ("quadratic_function_50_inputs_5_active_dims", "y", 50, 5),
        ("quadratic_function_100_inputs_2_active_dims", "y", 100, 2),
        ("quadratic_function_100_inputs_5_active_dims", "y", 100, 5),
    ],
    "real_datasets": [
        ("naca0012", "lift", 18, 3),
        ("hiv", "y_3400", 27, 3),
        ("onera_m6", "lift", 50, 3),
        ("elliptic_pde", "y_long", 100, 3),
    ],
}

subplot_dimensions = {
    "quadratic_functions": ((4, 2), (4.7, 7)),
    "real_datasets": ((2, 2), (4.7, 3.5)),
}

metric_labels = {
    "training_duration": "TD (s)",
    "r_squared": "$R^2$",
    "mlppd": "MLPPD",
    "mfsa": "MFSA",
}

dataset_labels = {
    "naca0012": "NACA0012",
    "hiv": "HIV",
    "onera_m6": "ONERA M6",
    "elliptic_pde": "Elliptic PDE",
}

model_labels = {
    "bfs": "B-FS",
    "moas": "MO-AS",
    "bgp": "B-GP",
}

# Matplotlib & Seaborn Configuration

plt.rcParams.update({"font.size": 8, "text.usetex": False, "font.serif": "Arial"})
sns.set(style="ticks")
FONT_SIZE = 8
plt.rc("font", size=FONT_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc("legend", fontsize=FONT_SIZE)  # legend fontsize
plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title


# Make the two plots (one per dataset group) for each metric
def plot_metric(metric_name, results_df):
    # One figure per group of datasets
    for dataset_group, datasets in dataset_groups.items():
        # Figure
        fig = plt.figure(figsize=subplot_dimensions[dataset_group][1])

        # Gridspec and axes
        gs = GridSpec(
            subplot_dimensions[dataset_group][0][0] + 1,
            subplot_dimensions[dataset_group][0][1],
            height_ratios=subplot_dimensions[dataset_group][0][0] * [8] + [1],
        )
        axes = [
            fig.add_subplot(gs[i, j])
            for i in range(subplot_dimensions[dataset_group][0][0])
            for j in range(subplot_dimensions[dataset_group][0][1])
        ]

        # Create one subplot per dataset
        for (
            i,
            (dataset_name, output_name, dim_input_space, dim_feature_space),
        ) in enumerate(datasets):
            dataset_rows = results_df[
                (results_df["dataset_name"] == dataset_name)
                & (results_df["output_name"] == output_name)
                & (results_df["dim_feature_space"] == dim_feature_space)
            ]

            # Retrieve current axis
            ax = axes[i]

            # Do the actual plot
            sns.boxplot(
                x="num_training_samples",
                y=metric_name,
                hue="model_name",
                data=dataset_rows,
                ax=ax,
                linewidth=0.5,
                width=0.7,
                flierprops=dict(markerfacecolor="0.50", markersize=2),
            )

            # Add a grid for readability
            ax.grid()

            # Remove legends from plots
            legend_handles, legend_labels = ax.get_legend_handles_labels()
            ax.get_legend().remove()

            # Only set an x-axis label for the bottom subplots
            if (
                i
                >= (subplot_dimensions[dataset_group][0][0] - 1)
                * subplot_dimensions[dataset_group][0][1]
            ):
                ax.set_xlabel("$n$")
            else:
                ax.set_xlabel(None)

            # Only set an x-axis label for the leftmost plots
            if i % subplot_dimensions[dataset_group][0][1] == 0:
                ax.set_ylabel(metric_labels[metric_name])
            else:
                ax.set_ylabel(None)

            # Change formatting for training_duration
            if metric_name == "training_duration":
                ax.yaxis.set_major_formatter(
                    ticker.FuncFormatter(lambda x, pos: f"{x:.1E}")
                )
                ax.set_yscale("log")

            # Filter out extreme MLPPD values
            if metric_name == "mlppd":
                # Set y lims as if MOAS results weren't there
                filtered_rows = dataset_rows[dataset_rows["model_name"] != "moas"]
                ymin = filtered_rows["mlppd"].min()
                ymax = filtered_rows["mlppd"].max()
                ax.set_ylim(ymin, ymax)

                # If we cut the plot for readability we need a data table
                if any(dataset_rows["mlppd"] < ymin):
                    print(dataset_name)
                    table = ""
                    for dataset_split_random_seed in filtered_rows[
                        "dataset_split_random_seed"
                    ].unique():
                        for model_name in sorted(dataset_rows["model_name"].unique()):
                            for num_training_samples in sorted(
                                dataset_rows["num_training_samples"].unique()
                            ):
                                value = dataset_rows["mlppd"][
                                    (
                                        dataset_rows["dataset_split_random_seed"]
                                        == dataset_split_random_seed
                                    )
                                    & (
                                        dataset_rows["num_training_samples"]
                                        == num_training_samples
                                    )
                                    & (dataset_rows["model_name"] == model_name)
                                ]
                                table += f" & {value.to_numpy()[0]:.2f}"
                            table += "\n"
                    print(table)

            # Set title to dataset name, input dim., and FS dim.
            base_title = f"$d = {dim_input_space}$, $m = {dim_feature_space}$"
            if dataset_group == "quadratic_functions":
                ax.set_title(base_title)
            else:
                ax.set_title(f"{dataset_labels[dataset_name]} ({base_title})")

        # Add a single legend for all subplots
        lax = fig.add_subplot(gs[subplot_dimensions[dataset_group][0][0], :])
        legend_labels = [model_labels[label] for label in legend_labels]
        lax.legend(
            legend_handles,
            legend_labels,
            mode=None,
            ncol=3,
            borderaxespad=0,
            loc="upper center",
        )
        lax.axis("off")

        # Tight layout
        plt.tight_layout()

        # Save figure
        # plt.savefig(
        #     figures_path / f"{dataset_group}_{metric_name}.pdf", format="pdf",
        # )
        plt.savefig(
            figures_path / f"{dataset_group}_{metric_name}.eps", format="eps",
        )

        # Close the figure to free up memory
        plt.close()
