import numpy as np

from bsdr.bfs import (
    train_bfs_model,
    predict_bfs_model,
    get_w_draws_bfs,
)
from bsdr.moas import (
    train_moas_model,
    predict_moas_model,
    get_w_draws_moas,
)
from bsdr.bgp import (
    train_bgp_model,
    predict_bgp_model,
    get_w_draws_bgp,
)
from bsdr.utils import generic_validation_routine, load_dataset, save_case_results

#####################
# Main Run Function #
#####################


MODEL_ROUTINES = {
    "bfs": (train_bfs_model, predict_bfs_model, get_w_draws_bfs),
    "moas": (train_moas_model, predict_moas_model, get_w_draws_moas),
    "bgp": (train_bgp_model, predict_bgp_model, get_w_draws_bgp),
}


def run_case(doe_name, case_number, process_parameters, case_parameters):
    # Retrieve relevant data
    model_name = case_parameters["model_name"]
    (training_routine, prediction_routine, get_w_draws) = MODEL_ROUTINES[model_name]
    training_parameters = process_parameters["training_parameters"][model_name]
    validation_parameters = process_parameters["validation_parameters"]

    # Load dataset
    x, y, dy_dx = load_dataset(
        case_parameters["dataset_name"], case_parameters["output_name"]
    )
    training_indices = np.array(case_parameters["training_indices"])
    all_indices = np.arange(x.shape[0])
    validation_indices = np.setdiff1d(all_indices, training_indices)

    # The dimension of the feature space needs to be copied in the training parameters
    training_parameters["dim_feature_space"] = case_parameters["dim_feature_space"]

    # Prepare training and validation sets
    training_data = {"x": x[training_indices], "y": y[training_indices]}
    validation_data = {"x": x[validation_indices], "y": y[validation_indices]}

    # Train model
    training_artifacts = training_routine(training_data, training_parameters)

    # Validate model
    validation_artifacts = generic_validation_routine(
        training_data,
        training_parameters,
        training_artifacts,
        prediction_routine,
        validation_data,
        validation_parameters,
        get_w_draws,
        dy_dx,
    )

    # Save results
    save_case_results(
        doe_name, case_number, case_parameters, training_artifacts, validation_artifacts
    )
