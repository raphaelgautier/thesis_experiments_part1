# Thesis Experiments - Part 1

This repository contains the implementation of experiment 1.1 from the thesis entitled "Bayesian, Gradient-Free, and Multi-Fidelity Supervised Dimension Reduction Methods For Surrogate Modeling Of Expensive Analyses With High-Dimensional Inputs".

## Setup

The setup instructions assume you are using anaconda/miniconda. Please note that the implementations of all models heavily rely on [Google's JAX framework](https://github.com/google/jax), which is only supported on Linux/MacOS/WSL. 

 - clone this repository and `cd` to the project root
 - create a conda environment using the provided `environment.yml` file (`conda env create --file=environment.yml`)
 - activate this environment (its default name is `bayesian_dimension_reduction`)
 - install this project as a library (`pip install -e .`)
 - finally, clone and install [this fork of pymanopt](https://gitlab.com/raphael.gautier/pymanopt)

## Run the code

The file `.config.example` should be copied as `.config` and the relevant configuration parameters should be filled. Configuration parameters under `[doe_scheduler]` can be ignored if only standalone cases are run instead of a DOE. The `[data]/data_dir` parameter should point to a directory that contains the data associated with this code. For example, you may use the data directory located [here](https://gitlab.com/raphael.gautier-papers/bayesian-supervised-dim-reduction-data). That repository contains all the data generated and used to create the plots presented in the paper.

The script `run_one_case.py` is the main point of entry. It features a set of parameters that can be modified to run a particular case. Running this script produces an `hdf5` file in a subfolder of the `results` directory that contains all relevant training and validation artifacts. `hdf5` files may be programatically opened, e.g. using [h5py](https://www.h5py.org/) or browsed using a utility such as [HDFView](https://www.hdfgroup.org/downloads/hdfview/).

## Repository Content

```python
.
├── bsdr
│   ├── __init__.py         # config file is read here 
│   ├── bfs.py              # implementation of the proposed approach
│   ├── bgp.py              # implementation of a fully Bayesian GP
│   ├── moas.py             # implementation of the MO-AS method
│   ├── run_case.py         # script used to run a DOE case
│   └── utils.py            # library file containing utility functions
├── scripts                 
│   ├── doe                 # scripts used to run parametric studies
│   ├── example                 
│   │   └── run_one_case.py # simple example, start here
│   └── plotting            # scripts used to generate figures
├── .config.example         # copy to `.config` and edit
├── .gitignore              # gitignore
├── environment.yml         # use to install the relevant environment
├── README.md               # this file
└── setup.py                # install as a library (required to run scripts)
```

## Related Repositories

 - [Thesis Experiments Part 2](https://gitlab.com/raphaelgautier/thesis_experiments_part2)
 - [Thesis Experiments Part 3](https://gitlab.com/raphaelgautier/thesis_experiments_part3)
 - [Thesis Experiments Part 4](https://gitlab.com/raphaelgautier/thesis_experiments_part4)